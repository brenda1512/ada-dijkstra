#ifndef _CLAEDGE_H
#define _CLAEDGE_H

#include "includes.h"

template<class G>
struct _claEdge
{
    typedef typename G::E E;
    typedef typename G::claNode claNode;
    claNode * varNodes[2];
    E varData;
    bool varDirection; //0 (bidireccional) 1(solo una direccion)

};


#endif // _CLAEDGE_H

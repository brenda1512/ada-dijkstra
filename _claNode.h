#ifndef _CLANODE_H
#define _CLANODE_H

#include "includes.h"

template<class G>
struct _claNode
{
    typedef typename G::N N;
    typedef typename G::claEdge claEdge;
    vector<claEdge *> varEdges;
    N varData;
};


#endif // _CLANODE_H

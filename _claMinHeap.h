#ifndef _CLAMINHEAP_H
#define _CLAMINHEAP_H
#include<iostream>
#include<vector>
#include "includes.h"

using namespace std;

class _claMinHeap
{
    vector<pair<int,int>> min;
    void BubbleDown(int pos);
    void BubbleUp();
public:
    _claMinHeap();
    void Insert(pair<int, int> newValue);
    pair<int,int> GetMin();
    void DeleteMin();
    void Delete(pair<int, int> value);
    void Print();
    pair<int, int> ExtractMin();
    bool isEmpty();
};

_claMinHeap::_claMinHeap()
{
}
bool _claMinHeap::isEmpty()
{
    return min.empty();
}

void _claMinHeap::BubbleUp()
{
    int last = min[min.size()-1].second;
    int pos = min.size()-1;
    while(pos > 0 && min[pos/2].second > last)
    {
        swap(min[pos],min[pos/2]);
//        pair<int, int> temp = min[pos/2];
//        min[pos] = min[pos/2];
//        min[pos/2] = temp;
        pos = pos/2;
    }
}


void _claMinHeap::Insert(pair<int, int> newValue)
{
    if(min.empty())
    {
        pair<int, int> ini;
        ini.first = -1;
        ini.second = -2;
        min.push_back(ini);
        min.push_back(newValue);
    }
    else
    {
        min.push_back(newValue);
        BubbleUp();
    }
}
pair<int, int> _claMinHeap::GetMin()
{
    pair<int, int> minimum = min[1];
    return minimum;
}

void _claMinHeap::BubbleDown(int pos)
{

    int smallest = pos;
    if(2*pos < min.size() && min[smallest].second > min[2*pos].second)
    {
        smallest = 2*pos;
    }

    if(2*pos+1 < min.size() && min[smallest].second > min[2*pos+1].second)
    {
        smallest = 2*pos+1;
    }
    if (smallest != pos) {
        swap(min[pos], min[smallest]);
        BubbleDown(smallest);
    }

}
void _claMinHeap::Delete(pair<int, int> value)
{
    int index = 0;
    for (int i = 1; i < min.size(); i++) {
        if (min[i] == value) {
            index = i;
            break;
        }
    }
    min[index] = min[min.size() - 1];
    min.pop_back();
    BubbleDown(index);
}


void _claMinHeap::DeleteMin()
{
    pair<int, int> minimum = min[1];
    min[1] = min[min.size()-1];
    min.pop_back();
    BubbleDown(1);
}
pair<int,int> _claMinHeap::ExtractMin()
{
    pair<int, int> minimum = min[1];
    min[1] = min[min.size()-1];
    min.pop_back();
    BubbleDown(1);
    return minimum;
}

void _claMinHeap::Print()
{

    for(int i=1 ; i<min.size() ; i++)
    {
        cout<<i<<":("<<min[i].first<<","<<min[i].second<<")"<<endl;
    }
    cout<<endl;
}

#endif // _CLAMINHEAP_H

#ifndef _CLAGRAPH_H
#define _CLAGRAPH_H

#include "includes.h"
#include "_claEdge.h"
#include "_claNode.h"

using namespace std;

template <class _N, class _E> // _N : Node datatype, _E : Edge datatype
class _claGraph
{
    public:
        typedef _claGraph<_N,_E> self;
        typedef _claNode<self> claNode;
        typedef _claEdge<self> claEdge;
        typedef _N N;
        typedef _E E;

        vector<claNode*> varNodes;
        vector<claEdge*> varEdges;

        _claGraph (int num_nodos)
        {
            for(int i=1;i<=num_nodos;i++)
            {
                metInsertNode(i);
            }
        }

        bool metInsertNode(N);
        bool metInsertEdge(N,N,E,bool);
        bool metDeleteNode(N);
        bool metDeleteEdge(N,N,E);
        int metFindNode(N);
        void metPrint();
};

template <class _N, class _E>
int _claGraph<_N,_E>::metFindNode(N parData)
{
    int i=0;
    for(auto & varElem : varNodes)
    {
        if(varElem->varData == parData)
            return i;
        i++;
    }
    return -1;
}


template <class _N,class _E>
bool _claGraph<_N,_E>::metInsertNode(_N parData)
{
    if(metFindNode(parData)!=-1)
        return false;

    auto varNode = new claNode();
    varNode->varData = parData;
    varNodes.push_back(varNode);

    return 1;
}

template <class _N,class _E>
bool _claGraph<_N,_E>::metInsertEdge(_N parDataStart, _N parDataEnd, _E parData, bool varDirection)
{

    int varPosStart = metFindNode(parDataStart);
    int varPosEnd = metFindNode(parDataEnd);
    if(varPosStart == -1 or varPosEnd == -1)
        return false;

    for(auto & varElem : varNodes.at(varPosStart)->varEdges)
    {
        if(varElem->varNodes[0]->varData == parDataEnd or varElem->varNodes[1]->varData == parDataEnd)
            return true;
    }

    auto varEdge = new claEdge();

    varEdge->varData = parData;
    varEdge->varDirection = varDirection;
    varEdge->varNodes[0] = varNodes.at(varPosStart);
    varEdge->varNodes[1] = varNodes.at(varPosEnd);

    varNodes.at(varPosStart)->varEdges.push_back(varEdge);
    varNodes.at(varPosEnd)->varEdges.push_back(varEdge);
    varEdges.push_back(varEdge);
    return true;

}
template <class _N,class _E>
bool _claGraph<_N,_E>::metDeleteNode(_N parData)
{
    int varPosNode = metFindNode(parData);
    if(varPosNode == -1)
        return false;

    for(auto & varElem : varNodes.at(varPosNode)->varEdges)
    {
        metDeleteEdge(varElem->varNodes[0]->varData, varElem->varNodes[1]->varData, varElem->varData);
    }

    varNodes.erase(varNodes.begin()+varPosNode);
    return true;
}

template <class _N,class _E>
bool _claGraph<_N,_E>::metDeleteEdge(_N parDataStart, _N parDataEnd, _E parData)
{
    int varPosStart = metFindNode(parDataStart);
    int varPosEnd = metFindNode(parDataEnd);

    if(varPosStart == -1 or varPosEnd == -1)
        return false;

    int i=0;
    for(auto & varElem : varNodes.at(varPosStart)->varEdges)
    {
        if(varElem->varData == parData and varElem->varNodes[1]->varData == parDataEnd)
        {
            (varNodes.at(varPosStart))->varEdges.erase(varNodes.at(varPosStart)->varEdges.begin()+i);
            break;
        }
        i++;
    }
    i=0;
    for(auto & varElem : varNodes.at(varPosEnd)->varEdges)
    {
        if(varElem->varData == parData and varElem->varNodes[0]->varData == parDataStart)
        {
            varNodes.at(varPosEnd)->varEdges.erase(varNodes.at(varPosEnd)->varEdges.begin()+i);;
            break;
        }
    }
    return true;
}

template <class _N,class _E>
void _claGraph<_N,_E>::metPrint()
{
    for(auto & varElem : varNodes)
    {
        cout << varElem->varData << " :\t";
        for(auto & varElem2 : varElem->varEdges)
        {
            cout << "(" << varElem2->varNodes[0]->varData << ", " << varElem2->varNodes[1]->varData << ", " << varElem2 ->varData << ")" << "\t";
        }
        cout << endl;
    }
/*	cout << "graph myGrafo{" <<endl;
	for(auto & varElem :varEdges)
	{
		cout << varElem->varNodes[0]->varData << " -- " << varElem->varNodes[1]->varData<<endl;
	}
	cout << "}";*/
}


#endif // _CLAGRAPH_H
